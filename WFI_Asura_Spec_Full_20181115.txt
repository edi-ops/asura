Asura - Series WFI Custom Feed						
						
KEY						
* = primary key						
				
						
Element Name	DataType	Format	Max Width	Lookup TYPEGROUP	Field Description	
BOND_ActFlag	Char	Char String	1	ACTION	Record Level Action Status	
BOND_Created	Date	yyyy/mm/dd	10		Creation date of record at EDI	
BOND_Changed	Date	yyyy/mm/dd	10		Last Changed date of record at EDI	
*SecId	Integer	32 bit	10		Security linking key	
ISIN	Char	Char String	12		ISIN from SCMST for reference purposes	
BondType	Char	Char String	10	BONDTYPE	Type of Debt Instrument	
DebtMarket	Char	Char String	10	DEBTMKT	"The Debt Market classification system indicating the currency and country of issue by an issuer (Yankee, Bulldog etc.)"	
DebtCurrency	Char	Char String	3	CUREN	ISO Currency Code	
NominalValue	Decimal	19.17	19			
IssueDate	Date	yyyy/mm/dd	10			
IssueCurrency	Char	Char String	3	CUREN	Debt Currency Code	
IssuePrice	Decimal	19.17	19			
IssueAmount	Decimal	19.17	19		Issue Amount in millions	
IssueAmountDate	Date	yyyy/mm/dd	10			
OutstandingAmount	Decimal	19.17	19		Outstanding Amount in millions	
OutstandingAmountDate	Date	yyyy/mm/dd	10	Effective date of the Outstanding Amount		
InterestBasis	Char	Char String	10	INTTYPE	"Denotes interest type ie Fixed, Floating"	
InterestRate	Decimal	19.17	19		Interest Rate applicable to the debt security	
InterestAccrualConvention	VarChar	Char String	10	INTACCRUAL	Interest Accrual Convention ie 30/360	
InterestPaymentFrequency	VarChar	Char String	3	DIVPERIOD		
IntCommencementDate	Date	yyyy/mm/dd	10	The commencement date for interest accrual		
OddFirstCoupon	Char	Char String	1	ODDCP	Flag to denote First coupon payment is Long or Short	
FirstCouponDate	Date	yyyy/mm/dd	10		The First coupon payment date	
OddLastCoupon	Char	Char String	1	ODDCP	Flag to denote Last coupon payment is Long or Short	
LastCouponDate	Date	yyyy/mm/dd	10		The Last coupon payment date before maturity	
InterestPayDate1	VarChar	Char String	4			
InterestPayDate2	VarChar	Char String	4			
InterestPayDate3	VarChar	Char String	4			
InterestPayDate4	VarChar	Char String	4			
DomesticTaxRate	Decimal	19.17	19		Witholding Tax on Interest for Resident Individual Bondholder	
NonResidentTaxRate	Decimal	19.17	19		Witholding Tax on Interest for Non-resident Individual Bondholder	
FRNType	VarChar	Char String	10	FRNTYPE	Denotes Floating Rate Note type	
FRNIndexBenchmark	VarChar	Char String	10	FRNINDXBEN	Indicates the Index which forms the benchmark for FRNs or Index Linked securities	
FrnMargin	Decimal	19.17	19		The margin relative to the index benchmark.	
FrnMinInterestRate	Decimal	19.17	19		Minimum interest rate for FRNs whose interest rates are range bound	
FrnMaxInterestRate	Decimal	19.17	19		Maximum interest rate for FRNs whose interest rates are range bound	
Rounding	Integer	32 bit	10		These values apply to the rounding applied to the interest rate and the figure denotes the number shown after the decimal place.	
Series	VarChar	Char String	50			
Class	VarChar	Char String	50			
OnTap	Char	Char String	1	BOOLEAN		Indicates if the issue is on tap.  Primarily for EMTN's and expected Tranches.
MaximumTapAmount	Decimal	19.17	19		Maximum tap amount permissible as per the prospectus	
TapExpiryDate	Date	yyyy/mm/dd	10		Last date by which the tap should be completed by the issuer	
Guaranteed	Char	Char String	1	BOOLEAN	Indicates if a security has the benefit of a Guarantee. This is usually either a Parent Company or Government	
SecuredBy	Char	Char String	1	YNBLANK	"Denotes that the debt is secured, usually against property"	
SecurityCharge	VarChar	Char String	5	ASSETBKD	Indicates if a security is Asset or Mortgage Backed	
Subordinate	Char	Char String	1	YNBLANK	Indicates whether a security has Subordinated Debt. These pay interest and principal to investors secondary after all full payment to Senior Debt.	
SeniorJunior	VarChar	Char String	1	SENJUN 	Indicates either Senior or Junior debt. Senior has first claim on collateral to be used for interest and principal payments and Junior second claim.	
WarrantAttached	Char	Char String	1	YNBLANK		
MaturityStructure	Char	Char String	1	DEBTSTATUS	"Identifies if the bond is Convertible, Redeemable or Exchangeable, details of which are captured separately"	
Perpetual	Char	Char String	1	PERPETUAL	Indicator for non maturing securities	
MaturityDate	Date	yyyy/mm/dd	10		The date on which the security redeems. The security is then cancelled.	
MaturityExtendible	Char	Char String	1	YNBLANK	Flag indicating if the bond maturity can be extended	
Callable	Char	Char String	1	YNBLANK		
Puttable	Char	Char String	1	YNBLANK		
Denomination1	Decimal	19.17	19			
Denomination2	Decimal	19.17	19			
Denomination3	Decimal	19.17	19			
Denomination4	Decimal	19.17	19			
Denomination5	Decimal	19.17	19			
Denomination6	Decimal	19.17	19			
Denomination7	Decimal	19.17	19			
MinimumDenomination	Decimal	19.17	19		Value of the Minimum Tradeable amount	
DenominationMultiple	BigINT	64 bit			Trading denomination multiples	
Strip	Char	Char String	3	STRIP	Denotes securities usually backed by the treasury in which coupons are sold separately from principal	
StripInterestNumber	Integer	32 bit	10			
BondSrc	Char	Char String	2	BONDSRC		
MaturityBenchmark	Char	Char String	10	MATBNHMRK	Index which will be the benchmark for the purposes of calculating the Maturity value	
ConventionMethod	VarChar	Char String	10	CMETHOD	Additional method used in interest accrual convention	
FrnInterestAdjFreq	VarChar	Char String	10	DIVPERIOD	Frequency with which the FRN Interest would be adjusted.  It may or may not be with the same frequency as interest payment.	
InterestBusDayConv	VarChar	Char String	10	INTBDC	Convention used to calculate the actual payment date in case the Interest Payment falls on a Holiday.	
InterestCurrency	Char	Char String	3	CUREN	"Dual Currency bonds are Issued in currency X, pay interest in currency Y and could redeem in currency Z.  Decided to introduce the concept after seeing several examples."	
MaturityBusDayConv	VarChar	Char String	10	INTBDC	Convention used to calculate the actual payment date in case the Maturity Payment falls on a Holiday.	
MaturityCurrency	Char	Char String	3	CUREN	"Dual Currency bonds are Issued in currency X, pay interest in currency Y and could redeem in currency Z.  Decided to introduce the concept after seeing several examples."	
TaxRules	VarChar	Char String	10	TAXRULE	To allow for population of Tax Rules available for the security.  Eg. TEFRA C or D.	
VarInterestPaydate	Char	Char String	1	YNBLANK	"It has been noted that there are many bonds where the frequency of interest payment is fixed but its date is not because it could be dependent on certain events (eg. Dividend), or formulaes (i.e. set of linked dates).  In order to indicate to the end user that the frequency is known but not the exact date this field has been introduced.  Details of the calculation would be pasted into the notes. "	
PriceAsPercent	Decimal	19.17	19		Issue Price expressed as a percentage	
PayOutMode	Char	Char String	1	PAYOUT	"Value indicating, if the payout will be and interest, dividend or a combination of both"	
Cumulative	Char	Char String	1	BOOLEAN	Flag indicating if the interest is payable 	
MatPrice	Decimal	19.17	19		Maturity Price	
MatPriceAsPercent	Decimal	19.17	19		Maturity price expressed as a percentage	
SinkingFund	Char	Char String	1	YNBLANK		Sinking fund
GovCity	VarChar	Char String	50		Identifies the applicability of City laws. 	
GovState	VarChar	Char String	50		Identifies the applicability of State laws. 	
GovCntry	VarChar	Char String	50		Identifies the applicability of Country laws. 	
GovLaw	VarChar	Char String	50		The law which governs the security	
GovLawNotes	Memo	Text No Carriage Return	1073741823		The law which governs the security	
Municipal	Char	Char String	1	YNBLANK	Flag indicating if a security is municipal	
PrivatePlacement	Char	Char String	1	YNBLANK	Flag indicating if a security is a private placement	
Syndicated	Char	Char String	1	YNBLANK	Flag indicating if a security is syndicated	
Tier	TinyInt	Integer 1	3		Tier levels of capital requirement	
UppLow	Char	Char String	1	UPPLOW	Sub division of tier structure	
Collateral	Char	Char String	1	YNBLANK	Denotes security is collateralized	
CoverPool	Char	Char String	10	COVERPOOL	Covered pool underlying asset	
PikPay	Char	Char String	1	YNBLANK	Notifies of payment in kind	
YieldAtIssue	Decimal	19.17	19		Original Yield at Issue	
CoCoTrigger	Char	Char String	10	COCOTRIG	Contingent Convertible Trigger. The lookup will identify the relevant trigger applicable upon which the security would be converted.	
CoCoAct	Char	Char String	10	COCOACTION	"Contingent Convertible Action. A lookup to state the action that would be taken on the bond when Contingent Convertible (CoCo) trigger event happens.  Eg. Bail-in, Conversion"	
CoCoNonViability	Char	Char String	10	NONVIABLE	The lookup will state the action that would be taken on a CoCo when the Regulator enforces  the non-viability option on the bank.	
Taxability	Char	Char String	10	TAXABILITY	A lookup indicating the nature of Tax Free status of the bond.	
LatestAppliedINTPYAnlCpnRateDate	Date	yyyy/mm/dd	10		Latest Applied Interest Payment Annual Coupon Rate Date	
LatestAppliedINTPYAnlCpnRate	VarChar	Char String	20		Latest Applied Interest Payment Annual Coupon Rate	
FirstAnnouncementDate	Date	yyyy/mm/dd	10			
PerformanceBenchmarkISIN	Char	Char String	12			
InterestGraceDays	Integer	32 bit	3		"In case the issuer is unable to pay interest on scheduled date, it may invoke the Grace period clause to pay within that period and not be classified as in default."	
InterestGraceDayConvention	VarChar	Char String	10	INTGRCONV	Business or Calendar days used to count the grace period.	
MatGraceDays	Integer	32 bit	3		"In case the issuer is unable to pay redemption amount on maturity date, it may invoke the Grace period clause to pay within that period and not be classified as in default."	
MatGraceDayConvention	VarChar	Char String	10	MATGRCONV	Business or Calendar days used to count the grace period.	
ExpMatDate	Date	yyyy/mm/dd	10		Expected Maturity Date	
GreenBond	Char	Char String	1	YNBLANK	Green Bond Flag	
IndicativeIssAmtDate	Date	yyyy/mm/dd	10		Indicative Issue Amount Date	
IndicativeIssAmt	VarChar	Char String	20		Indicative Issue Amount in Millions	
CoCoCapRatioTriggerPercent	VarChar	Char String	20		CoCo Capital Ratio Trigger Percent	
ZCMarker	Char	Char String	1	YNBLANK	Zero Coupon Marker	
BOND_Notes	Memo	Text No Carriage Return	1073741823		Notes applicable.	
SCMST_ActFlag	Char	Char String	1	ACTION	Record Level Action Status	
SCMST_Created	Date	yyyy/mm/dd	10		Creation date of record at EDI	
SCMST_Changed	Date	yyyy/mm/dd	10		Last Changed date of record at EDI	
ParentSecID	Integer	32 bit	10		SecID of parent line for items such as Strip issues	
SectyCD	Char	Char String	3	SECTYPE	Identifies Fixed Income Instruments	
SecurityDesc	VarChar	Char String	70			
StatusFlag	Char	Char String	1	STATUSFLAG	Inactive at the global level else security is active. Not to be confused with delisted which is inactive at the exchange level	
PrimaryExchgCD	Char	Char String	6	EXCHG	Exchange code for the primary listing (empty if unknown)	
CurenCD	Char	Char String	3	CUREN	The applicable currency that the bond is denominated in	
ParValue	Decimal	19.17	19		"Indicates the nominal values that the debt security is issued in - eg USD1000, USD10000"	
USCode	Char	Char String	9			
CommonCode	Char	Char String	15			
Holding	Char	Char String	10	HOLDING	"Denotes Bearer, Registered etc."	
StructCD	Char	Char String	10	STRUCTCD		
WKN	Char	Char String	6			
REGS144A	Char	Char String	10	REGS144A		
CFI	Char	Char String	10		ISO CFI - Classification of Financial Instruments. Alphabetical code consisting of 6 letters. 	
CIC	VarChar	Char String	4	Complementary Identification Code		
StatusReason	VarChar	Char String	50			
IssuePrice	Decimal	19.17	19			
PaidUpValue	Decimal	19.17	19			
Voting	Char	Char String	1	VOTING	Voting details lookup	
FYENPPDate	Date	yyyy/mm/dd	10			
VALOREN	Char	Char String	20			
SharesOutstanding	VarChar	Char String	25		Most recent known shares outstanding figure	
SharesOutstandingDate	Date	yyyy/mm/dd	10		Shares outstanding effective date	
UmprgID	Integer	32 bit	10			
NoParValue	Char	Char String	1	BOOLEAN	Flag indication NPV	
ExpDate	Date	yyyy/mm/dd	10			
VotePerSec	Decimal	19.17	19		Vottes per security	
FreeFloat	VarChar	Char String	15			
FreeFloatDate	Date	yyyy/mm/dd	10			
TreasuryShares	VarChar	Char String	25			
TreasurySharesDate	Date	yyyy/mm/dd	10			
FISN	VarChar	Char String	35		Financial Instrument Short Name	
SCMST_Notes	Memo	Text No Carriage Return	1073741823		Notes applicable.	
ISSUR_ActFlag	Char	Char String	1	ACTION	Record Level Action Status	
ISSUR_Created	Date	yyyy/mm/dd	10		Creation date of record at EDI	
ISSUR_Changed	Date	yyyy/mm/dd	10		Last Changed date of record at EDI	
IssID	Integer	32 bit	10			
SIC	Char	Char VarChar	10		Standard Industrial Classification code	
CIK	Char	Char VarChar	10		Central Index Key Code	
IssuerName	VarChar	Char String	70			
IndusID	Integer	32 bit	10	INDUS		
CntryofIncorp	Char	Char String	2	CNTRY		
FinancialYearEnd	char	Char String	4		Financial Year End date (DDMM)	
Shortname	VarChar	Char String	20			
Legalname	Char	Char String	1	YNBLANK	If Issuername above is the same as the Legal name	
Isstype	Char	Char String	10	ISSTYPE	"whether GOVERNMENT, CORPORATE, AGENCY etc."	
CntryofDom	Char	Char String	2	CNTRY		
StateofDom	Char	Char String	2			
LEI	VarChar	Char String	20	Legal Entity Identifier		
NAICS	VarChar	Char String	6	North American Industrial Classification System		
CntryIncorpNumber	VarChar	Char String	50	Country Incorporation Number. Unique country specific registration number of the company.		
ShellCompany	Char	Char String	1	YNBLANK	Flag to indicate the issuer is a shell company i.e. a company without active business operations or significant assets.	
ISSUR_Notes	Memo	Text No Carriage Return	1073741823		Notes applicable.	
						
	
						
Use YYYYMMDD_LOOKUP.txt to lookup any the meaning of coded data with an entry in Lookup TYPEGROUP column						
Notes on LOOKUP						
Combined lookup table where codes of a particular type are grouped by TypeGroup						
Join using the Typegroup of the coded field data and the coded data field content to get the lookup						
Only field containing coded date has the Typegroup colomn populated.						
This table also gives all the POSSIBLE VALUES.						
"Because it is combining multiple coded types, the format of the field must accomodate the largest possible code in the whole system hence the slight apparent incompatibility in length between to joining fields."						
						
						
----------						
Series WFI - LOOKUP - Combined Lookup Table -						
						
Element Name	DataType	Format	Max Width	Lookup TYPEGROUP	Field Description	
Actflag	Char	Char String	1	ACTION	Record Level Action Status	
Changed	Date	yyyy/mm/dd	10		Last Changed date of record at EDI	
*TypeGroup	Char	Char String	10	TYPEGROUP	Link between coded data and Combined Lookup Table	
*Code	Char	Char String	10		Code data value	
Lookup	VarChar	Char String	70		Lookup value for Code (previous field)	
